package org.webproject2018;


import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtil {

    /**
     *EntityManagerFactory(interface):
     *  Interfaccia utilizzata per interagire con il factory manager, entità per l'unità di persistenza.
     *
     *  facorymanager > (persistenceUnitName)
     */

    private static EntityManagerFactory factory;
    private static final String PERSISTENCE_UNIT_NAME="jcourse2018";

    private JPAUtil(){}

    public static EntityManagerFactory getEntityManagerFactory(String persistenceUnitName){
        if (persistenceUnitName==null)
             persistenceUnitName = PERSISTENCE_UNIT_NAME;
        if (factory==null)
            factory=Persistence.createEntityManagerFactory(persistenceUnitName);

        return factory;
    }

    public void shutdown(){ if(factory!= null){factory.close();}}
}
