package org.webproject2018.service;

import org.webproject2018.exceptions.UserRegistrationValidationException;
import org.webproject2018.exceptions.UserYetRegisteredException;
import org.webproject2018.exceptions.WrongCredentialsException;
import org.webproject2018.model.User;
import org.webproject2018.model.UserLoginDTO;
import org.webproject2018.model.UserRegistrationDTO;
import org.webproject2018.repository.UserDAO;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class UserService implements IUserService {

    UserDAO userDAO;

    public UserService(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    public User registration(UserRegistrationDTO userRegistrationDTO) throws UserYetRegisteredException {
        Long userId = userDAO.saveUser(userRegistrationDTO);
        User user = userDAO.getUserById(userId);
        return user;
    }

    public void registrationValidation(UserRegistrationDTO userRegistrationDTO)throws UserRegistrationValidationException {
         Map<String,String> errors= new HashMap<>();



        String regEXEmail="^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9]{2,})+.)+([a-zA-Z0-9]{2,})+$";
        if(!Pattern.compile(regEXEmail,Pattern.CASE_INSENSITIVE).matcher(userRegistrationDTO.getEmail()).matches())
            errors.put("email","l'email non è valida");

         //controllo ugualianza della password > se le password non coincidono metti nel messaggio di errore il seguente messaggio
        if(!userRegistrationDTO.getPassword().equals(userRegistrationDTO.getRepeatedPassword()))
           // System.out.println(userRegistrationDTO.getPassword()+" "+userRegistrationDTO.getRepeatedPassword()); => null!!
            errors.put("password","le password non coincidono");

        String regExPassword="((?=.*[0-9])([a-zA-Z]).{8,})";
        if(!Pattern.compile(regExPassword,Pattern.CASE_INSENSITIVE).matcher(userRegistrationDTO.getPassword()).matches())
            errors.put("password","la password non rispetta le specifiche:"+
                    "<ul>inserire almeno 8 caratteri</ul>" +
                    "<ul>inserire caratteri minuscoli e maiuscoli</ul>" +
                    "<ul>inserire caratteri speciali</ul>"
            );
         if(!errors.isEmpty()) {
             throw new UserRegistrationValidationException(errors);
         }
    }

    /**
     *Se user==null allora username o password errate
     */
    public User login(UserLoginDTO userLoginDTO) throws WrongCredentialsException {
        User user = userDAO.getUserByUsernameAndPassword(userLoginDTO);
        if (user == null)
            throw new WrongCredentialsException();
        return user;
    }

}
