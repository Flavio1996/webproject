package org.webproject2018.service;

import org.webproject2018.exceptions.UserRegistrationValidationException;
import org.webproject2018.exceptions.UserYetRegisteredException;
import org.webproject2018.exceptions.WrongCredentialsException;
import org.webproject2018.model.User;
import org.webproject2018.model.UserLoginDTO;
import org.webproject2018.model.UserRegistrationDTO;

public interface IUserService {

    User registration(UserRegistrationDTO userRegistrationDTO) throws UserYetRegisteredException;
    void registrationValidation(UserRegistrationDTO userRegistrationDTO) throws UserRegistrationValidationException;
    User login(UserLoginDTO userLoginDTO) throws WrongCredentialsException;

}
