package org.webproject2018.model;

import java.util.HashMap;
import java.util.Map;

public class ResponseDTO<T> {

    private T dtoData;  //<DTO>

    private Map<String,String> messages; // key-value



    public ResponseDTO(){
        messages=new HashMap<>();
    } //builder

    public T getDtoData() { return dtoData; }

    public void setDtoData(T dtoData) {
        this.dtoData = dtoData;
    }

    public Map<String, String> getMessage() {
        return messages;
    }

    public void setMessage(Map<String, String> message) {
        this.messages = message;
    }

    public void addMessage(String index,String message){ messages.put(index, message);} // method added
}
