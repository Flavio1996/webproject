package org.webproject2018.exceptions;

public class UserYetRegisteredException extends Throwable {
    public UserYetRegisteredException(){
        super("utente già registrato");
    }
}
