package org.webproject2018.exceptions;

public class WrongCredentialsException extends Throwable {
    public WrongCredentialsException(){
        super("username o password errati");
    }
}
