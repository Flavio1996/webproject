package org.webproject2018.exceptions;

import java.util.Map;

public class UserRegistrationValidationException extends Throwable{

    private Map<String,String> errors;

    /**
     * nel costruttore viene inizializzata una variabile che contiene il messaggio di errore del metodo registrationValidation
     * Infatti il costruttore dell'eccezione prende in Input il messaggio di "probabile" errore della registrationValidation
     */

    public UserRegistrationValidationException(Map<String,String> errors){
        super();
        this.errors=errors;
    }

    public Map<String, String> getErrors() { return errors; }

    public void setErrors(Map<String, String> errors) { this.errors = errors; }
}
