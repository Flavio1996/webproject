package org.webproject2018.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class Session {

    public static void createSession(HttpServletRequest req,String param){


        //ottieni la vecchia sessione e invalidala.
        HttpSession oldSession= req.getSession(false);//ritorna,se possibile la sessione esistente,altrimenti ritorna null
        if(oldSession != null){   // if session==null > non c'è bisogno di invalidarla perchè non esiste
            oldSession.invalidate();


            //crea una nuova sessione
            HttpSession newSession=req.getSession(true);//ritorna la sessione esistente, e se non esiste,te ne crea una nuova
            newSession.setAttribute("username",param);


            //intervallo di inattività
            newSession.setMaxInactiveInterval(5 * 60);
        }


    }


}
