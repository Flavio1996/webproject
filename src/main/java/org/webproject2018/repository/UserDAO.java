package org.webproject2018.repository;

import org.webproject2018.exceptions.UserYetRegisteredException;
import org.webproject2018.model.User;
import org.webproject2018.model.UserLoginDTO;
import org.webproject2018.model.UserRegistrationDTO;

public interface UserDAO {
    User getUserById(Long id);
    User getUserByUsername(String username);
    User getUserByUsernameAndPassword(UserLoginDTO userLoginDTO);
    Long saveUser(UserRegistrationDTO userRegistrationDTO) throws UserYetRegisteredException;
}
