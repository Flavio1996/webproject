package org.webproject2018.repository;

import org.webproject2018.exceptions.UserYetRegisteredException;
import org.webproject2018.model.User;
import org.webproject2018.model.UserLoginDTO;
import org.webproject2018.model.UserRegistrationDTO;

import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.Base64;


public class UserDaoImpl extends GenericDAO<User,Long> implements UserDAO{

    public UserDaoImpl(Class<User> entityClass) {
        super(entityClass);
    }

    @Override
    public User getUserById(Long id) {
        User user= findById(id);
        return user;
    }

    /**
     * @getSingleResult
     *esegui una query SELECT che restituisce un singolo risultato.
     *
     */

    @Override
    public User getUserByUsername(String username) {
        User user= null;

        TypedQuery<User> query= getEm().createQuery("Select u from User u where u.username=:username",User.class);
        query.setParameter("username",username);
        try {
            user = query.getSingleResult();
        }catch (NoResultException e){
            //
        }
        return user;
    }

    @Override
    public User getUserByUsernameAndPassword(UserLoginDTO userLoginDTO) {
        User user= null;
        String encryptedPassword=encryptPassword(userLoginDTO.getPassword());
        TypedQuery<User> query= getEm().createQuery("Select u from User u where u.username=:username and u.password=:password",User.class);
        query.setParameter("username",userLoginDTO.getUsername());
        query.setParameter("password",encryptedPassword);
        try{
            user= query.getSingleResult();
        }catch (NoResultException e){
            //
        }
        return user;
    }


    /**
     * @saveUser
     *metodo per salvare i dati dell'utente che si registra al'interno del DB.
     *
     *
     * manca il check del controllo sull'email: se l'utente si registra con una email già esistente viene registrato comunque
     */
    @Override
    public Long saveUser(UserRegistrationDTO userRegistrationDTO) throws UserYetRegisteredException {

        User user = getUserByUsername(userRegistrationDTO.getUsername());
        if(user==null)// se non trova l'username allora l'utente non esiste e va creato.
            user=new User();

        try {
            user.setName(userRegistrationDTO.getName());
            user.setSurname(userRegistrationDTO.getSurname());
            user.setEmail(userRegistrationDTO.getEmail());
            user.setUsername(userRegistrationDTO.getUsername());
            user.setPassword(encryptPassword(userRegistrationDTO.getPassword()));
            user.setCreatedBy("System");
            user.setCreatedDate(LocalDateTime.now());
            user.setActive(true);
            save(user);

        }catch (PersistenceException e){
            throw new UserYetRegisteredException();
        }
        return user.getId();
    }



    /**
     * algorithm="SHA-256".
     */

    private String encryptPassword(String password){
       String encryptedPassword=null;
       MessageDigest algorithm=null;
       try{
           algorithm= MessageDigest.getInstance("SHA-256");
           byte hash[]=algorithm.digest(password.getBytes(StandardCharsets.UTF_8));
           encryptedPassword=Base64.getEncoder().encodeToString(hash);
       }catch (NoSuchAlgorithmException e){
          e.printStackTrace();
       }
       return encryptedPassword;
    }
}
