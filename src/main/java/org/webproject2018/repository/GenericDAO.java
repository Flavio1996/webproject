package org.webproject2018.repository;


import org.webproject2018.JPAUtil;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;

/**
 *  FIXME
 *  param entityClass or entity!
 */

public class GenericDAO<T,ID extends Serializable> {


    /**
     * @EntityManager
     *
     * Interfaccia utilizzata per interagire con il contesto di persistenza.
     * Gestisce lo stato (o il ciclo di vita) persistente di un'entità.
     * em > è la porta che serve per acccedere ai dati nel database.
     */
    @PersistenceContext
    private EntityManager em;

    protected final Class<T> entityClass; // >User class


  /*
    private GenericDAO() {
        this.entityClass = null;
    }
    */


    /**
     * costruttore che inizializza la entityClass e che attraverso la factory crea un entityManger.
     */
    protected GenericDAO(Class<T> entityClass){
        System.err.println(".....GENERICDAO!");
        em= JPAUtil.getEntityManagerFactory("jcourse2018").createEntityManager();
        this.entityClass=entityClass;
    }


    public T findById(ID id){return em.find(entityClass,id);}

    public void remove(T entity){em.remove(entity);}

    /**
     * @persist = salva entity
     *
     * quando facciamo una modifica, per tutte le operazioni che non sono una select, dobbiamo dire all'em di
     * farle in transazione. In una transazione o tutte le operazioni sono state eseguite correttamente oppure è
     * come se non fosse stata eseguita nessuna operazione.
     */

    public void save(T entity){
        em.getTransaction().begin();
        em.persist(entity);
        em.getTransaction().commit();
        //em.close();  warn
    }



    public EntityManager getEm() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }
}
