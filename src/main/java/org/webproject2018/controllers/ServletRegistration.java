package org.webproject2018.controllers;

import com.google.gson.Gson;
import org.webproject2018.exceptions.UserRegistrationValidationException;
import org.webproject2018.exceptions.UserYetRegisteredException;
import org.webproject2018.model.ResponseDTO;
import org.webproject2018.model.User;
import org.webproject2018.model.UserRegistrationDTO;
import org.webproject2018.repository.UserDaoImpl;
import org.webproject2018.service.IUserService;
import org.webproject2018.service.UserService;
import org.webproject2018.utils.Session;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;



@WebServlet("/registration")
public class ServletRegistration extends HttpServlet {

    IUserService userService;

    @Override
    public void init() throws ServletException {
        super.init();
        userService = new UserService(new UserDaoImpl(User.class));
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        getServletContext().log("ServletRegistration get called");
        resp.setContentType("text/html;charset=UFT-8");
        resp.sendRedirect("html/registration.html");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        /**
         * @Buffer
         *Il buffer viene chiamato anche come memoria tampone o memoria intermediaria.
         * è una zona di memoria usata per compensare differenze di velocità nel
         * trasferimento o nella trasmissione di dati, oppure per velocizzare l'esecuzione
         * di alcune operazioni.
         * RB:esempio CPU e stampante
         *
         * @BufferedReader
         * Legge il testo da un flusso di input di caratteri,
         * memorizzando i caratteri in modo da fornire una lettura efficiente
         *
         * @(obj)reader=getReader()
         * Recupera il corpo della richiesta come dati carattere
         *
         * @Gson
         * Fornisce funzionalità per convertire oggetti Java in corrispondenti costrutti JSON e viceversa.
         *
         * @fromJson
         * Questo metodo deserializza la lettura Json dal lettore specificato in un oggetto della classe specificata
         * json >obj(T)
         */

        BufferedReader reader= req.getReader();
        Gson gson= new Gson();

        //String readerValue= reader.lines().collect(Collectors.joining());


        UserRegistrationDTO userRegistrationDTO=gson.fromJson(reader,UserRegistrationDTO.class);//mi prendo i parametri dal json - deserializzi
        ResponseDTO<User> responseDTO= new ResponseDTO<>();

        try{
            userService.registrationValidation(userRegistrationDTO);
            userService.registration(userRegistrationDTO);
            Session.createSession(req,userRegistrationDTO.getUsername());
            Cookie cookie= new Cookie("message","Benveuto");
            resp.addCookie(cookie);
            resp.sendRedirect("admin/homepage");
        }catch (UserYetRegisteredException e){
           responseDTO.addMessage("username","Utente già registrato!");
        }catch (UserRegistrationValidationException e) {
            responseDTO.setMessage(e.getErrors());
        }

        /**
         * @response.getWriter
         * restituisce un oggetto(out)PrintWriter che può inviare testo di caratteri al client.
         *
         * @toJson
         * toJson(Object src)
         * Questo metodo serializza l'oggetto specificato nella sua rappresentazione Json equivalente.
         * obj(T)> json
         *
         * @flush
         * questo metodo serve a scrivere tutto ciò che si trova sul buffer sul disco
         */

        PrintWriter out = resp.getWriter();

        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");

        out.write(gson.toJson(responseDTO)); // scrive nel buffer! L'oggetto che contiene i messaggi di errore viene convertito in un json serializziiiii
        out.flush();
        out.close();
    }

     /*User user = registration(req);

        if (user != null) {
            resp.sendRedirect("/admin/homepage");
        } else {
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/html/registration.html");
            PrintWriter out = resp.getWriter();
            out.println("<p>registrazione non effettuata, ricontrolla i dati inseriti</p>");
            rd.include(req, resp);
            out.close();
        }*/


   /* private User registration(HttpServletRequest req) {
        String name = req.getParameter("name");
        String surname = req.getParameter("surname");
        String email = req.getParameter("email");
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        String confirmPassword = req.getParameter("confirmPassword");

        if (!password.equals(confirmPassword)) {
            System.out.println("Password are not equals");
        }else {
            User user=null;
            UserRegistrationDTO userRegistrationDTO = new UserRegistrationDTO(name, surname, email, username, password);
            try {
                user= userService.registration(userRegistrationDTO);
            } catch (UserYetRegisteredException e) {
                e.printStackTrace();
            }return user;
        }return null;
    }*/
}






