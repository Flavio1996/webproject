$(document).ready(function(){ //eseguito dopo che la pagina è stata caricata

    $(".loginBtn").on('click', function() {
        $("#loginForm").submit(); //invia il modulo (come se si stesse facendo clic sul pulsante Invia)
    });

    $(".btnRegistra").on('click', function() {
        eseguiRegistrazione();

    });

});


/*
 ottengo il valore del campo attraverso l'id di input nella form html
*/

function eseguiRegistrazione() {
    var formData = {
        name: $("#name").val(),
        surname: $("#surname").val(),
        email: $("#email").val(),
        username: $("#username").val(),
        password: $("#password").val(),
        repeatedPassword: $("#repeatedPassword").val()
    };

    //chiamata ajax

    $.ajax({
        type:"POST",
        url:"/registration",
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        data: JSON.stringify(formData),
        success: getResponse
    });

    function getResponse(data) {
        $('.errorMessages').hide();
        if (data!=null) {
            if(data.messages!=null) {
                $('.errorMessages').empty();
                $('.errorMessages').show();
                $.each(data.messages, function(k, v) {
                    $('.errorMessages').append('<li><span>' + k + '</span> ' + v + '</li>');
                });
            }
        }
    }

}




/*
$(document).ready(function(){ //executed after the page has loaded

    $.ajax({
        type:"GET",
        url:"/admin/userinfo",
        success: function(data) {
            user = JSON.stringify(data);
            $('#credenziali').text(user.surname + " " + user.name);
        },
        dataType: 'json',
    });
});
*/